package common;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pageObject.homeObject;
import pageObject.loginObject;
import pageObject.reportIssueObject;
import pageObject.viewIssuesObject;

public class commonFuntions {
	// login
	public static void commonLogin() throws Exception {
		utils.driver.get(utils.URL);

		XSSFSheet ExcelDataLogIn = ExcelCommon_POI.setExcelFile(utils.excelData, utils.sheetLogin);
		String Username = ExcelCommon_POI.getCellData(1, 1, ExcelDataLogIn);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataLogIn);

		utils.driver.findElement(loginObject.txtUsername).sendKeys(Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(Password);
		utils.driver.findElement(loginObject.btnLogin).click();
		Thread.sleep(3000);
		Assert.assertEquals(true, utils.driver.getPageSource().contains("Logged in"));
	}

	// Logout
	public static void commonLogOut() throws InterruptedException {
		utils.driver.findElement(homeObject.mnuLogOut).click();
	}

	// Create Issue
	public static void commonReportIssue() throws Exception {
		// load report issue page
		utils.driver.findElement(homeObject.mnuReportIssue).click();
		Thread.sleep(3000);

		// Read from excel
		XSSFSheet ExcelDataReportIssue = ExcelCommon_POI.setExcelFile(utils.excelData, utils.sheetReportIssue);
		String category = ExcelCommon_POI.getCellData(1, 1, ExcelDataReportIssue);
		String reproducibility = ExcelCommon_POI.getCellData(1, 2, ExcelDataReportIssue);
		String severity = ExcelCommon_POI.getCellData(1, 3, ExcelDataReportIssue);
		String priority = ExcelCommon_POI.getCellData(1, 4, ExcelDataReportIssue);
		String platform = ExcelCommon_POI.getCellData(1, 5, ExcelDataReportIssue);
		String os = ExcelCommon_POI.getCellData(1, 6, ExcelDataReportIssue);
		String osVersion = ExcelCommon_POI.getCellData(1, 7, ExcelDataReportIssue);
		String assignee = ExcelCommon_POI.getCellData(1, 8, ExcelDataReportIssue);
		String summary = ExcelCommon_POI.getCellData(1, 9, ExcelDataReportIssue);
		String description = ExcelCommon_POI.getCellData(1, 10, ExcelDataReportIssue);
		String reproduce = ExcelCommon_POI.getCellData(1, 11, ExcelDataReportIssue);
		String addInfo = ExcelCommon_POI.getCellData(1, 12, ExcelDataReportIssue);

		// input info
		utils.driver.findElement(reportIssueObject.slbCategory).sendKeys(category);
		utils.driver.findElement(reportIssueObject.slbReproducibility).sendKeys(reproducibility);
		utils.driver.findElement(reportIssueObject.slbSeverity).sendKeys(severity);
		utils.driver.findElement(reportIssueObject.slbPriority).sendKeys(priority);
		utils.driver.findElement(reportIssueObject.txtPlatform).sendKeys(platform);
		utils.driver.findElement(reportIssueObject.txtOS).sendKeys(os);
		utils.driver.findElement(reportIssueObject.txtOSVersion).sendKeys(osVersion);
		utils.driver.findElement(reportIssueObject.slbAssignee).sendKeys(assignee);
		utils.driver.findElement(reportIssueObject.txtSummary).sendKeys(summary);
		utils.driver.findElement(reportIssueObject.txtDescription).sendKeys(description);
		utils.driver.findElement(reportIssueObject.txtReproduce).sendKeys(reproduce);
		utils.driver.findElement(reportIssueObject.txtAdditionalInfo).sendKeys(addInfo);
		utils.driver.findElement(reportIssueObject.btnSubmitReport).click();
		Thread.sleep(5000);
	}
	

	// Search Issue
	public static void commonSeachIssue() throws InterruptedException {
		// search issue
		utils.driver.findElement(viewIssuesObject.txtSearch).clear();
		utils.driver.findElement(viewIssuesObject.txtSearch).sendKeys("0948251919");
		utils.driver.findElement(viewIssuesObject.btnFilter).click();
		Thread.sleep(3000);
	}
	
	public static void commonDeleteIssue() throws InterruptedException {
		utils.driver.findElement(viewIssuesObject.chkAllBug).click();
		utils.driver.findElement(viewIssuesObject.slbAction).sendKeys("Delete");
		Thread.sleep(3000);
		utils.driver.findElement(viewIssuesObject.btnOK).click();
		WebDriverWait wait = new WebDriverWait(utils.driver, 30);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(viewIssuesObject.btnDelete));
		element.click();
		Thread.sleep(3000);

	}
}
