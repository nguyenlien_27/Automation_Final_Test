package common;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class OpenMultiBrowser {
	
	public static void multi_browser(String browser) throws Exception{
		//Firefox
		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", utils.firefoxDriver);
			utils.driver = new FirefoxDriver();
			utils.driver.manage().window().maximize();
			
		//Chrome	
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", utils.chromeDriver);
			utils.driver = new ChromeDriver();	
			utils.driver.manage().window().maximize();
	
		} else if (browser.equalsIgnoreCase("ie")) {
			 System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			  DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			  caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			  caps.setCapability("nativeEvents",false);
			  utils.driver = new InternetExplorerDriver(caps);			  			  
			  utils.driver.manage().window().maximize();
			  
		} else {
			throw new IllegalArgumentException("The Browser Type is undefined");
		}			
		utils.driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);				
	}	
}