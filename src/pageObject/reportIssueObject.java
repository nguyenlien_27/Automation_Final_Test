package pageObject;

import org.openqa.selenium.By;

public class reportIssueObject {
	public static By slbCategory = By.name("category_id");
	public static By slbReproducibility = By.name("reproducibility");
	public static By slbSeverity = By.name("severity");
	public static By slbPriority = By.name("priority");
	public static By txtPlatform = By.id("platform");
	public static By txtOS = By.id("os");
	public static By txtOSVersion = By.id("os_build");
	public static By slbAssignee = By.name("handler_id");
	public static By txtSummary = By.name("summary");
	public static By txtDescription = By.name("description");
	public static By txtReproduce = By.name("steps_to_reproduce");
	public static By txtAdditionalInfo = By.name("additional_info");
	public static By btnSubmitReport = By.xpath("//input[@value='Submit Report']");
}
