package pageObject;

import org.openqa.selenium.By;

public class loginObject {
	// log in items
	public static By txtUsername = By.name("username");
	public static By txtPassword = By.name("password");
	public static By btnLogin = By.xpath("//input[@value='Login']");

	public static By lSignUpNewAccount1 = By.xpath("html/body/div[4]/span[1]/a");

	// forget password items
	public static By lSignUpNewAccount = By.xpath("html/body/div[4]/span[1]/a");
	public static By lLostPass = By.xpath("html/body/div[4]/span[2]/a");
	public static By textEmail = By.name("email");
	public static By btnSubmit = By.xpath("//input[@value='Submit']");
	public static By lProceed = By.xpath("//a[contains(.,'Proceed')]");
}
