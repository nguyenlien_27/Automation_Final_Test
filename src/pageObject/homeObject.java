package pageObject;

import org.openqa.selenium.By;

public class homeObject {
	// Menu items
	public static By mnuViewIssues = By.linkText("View Issues");
	public static By mnuReportIssue = By.linkText("Report Issue");
	public static By mnuLogOut = By.xpath("html/body/table[2]/tbody/tr/td[1]/a[7]");
	public static By mnuMyAccount = By.linkText("My Account");
}
