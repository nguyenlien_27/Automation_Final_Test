package pageObject;

import org.openqa.selenium.By;

public class signupObject {
	public static By txtUserName = By.name("username");
	public static By txtEmail = By.name("email");
	public static By btnSignUp = By.xpath("html/body/div[2]/form/table/tbody/tr[5]/td/input");
}
