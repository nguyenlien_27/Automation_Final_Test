package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.OpenMultiBrowser;
import common.commonFuntions;
import common.utils;
import pageObject.homeObject;

public class tc4_searchForIssue {
	@Parameters("browser")
	@BeforeMethod
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
		commonFuntions.commonLogin();
	}

	@Test
	public void searchIssue() throws Exception {
		// view issues
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);

		// search issue
		commonFuntions.commonSeachIssue();

		if (!utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			ExcelCommon_POI.setCellData(5, 3, utils.excelData, utils.sheetMaster, "Pass");
		} else {
			commonFuntions.commonReportIssue();
			commonFuntions.commonSeachIssue();
			if (!utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
				ExcelCommon_POI.setCellData(5, 3, utils.excelData, utils.sheetMaster, "Pass");
			} else {
				ExcelCommon_POI.setCellData(5, 3, utils.excelData, utils.sheetMaster, "Fail");
			}
		}
	}

	@AfterMethod
	public void after() throws InterruptedException {
		commonFuntions.commonLogOut();
		utils.driver.close();
	}
}
