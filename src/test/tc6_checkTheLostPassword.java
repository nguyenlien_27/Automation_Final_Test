package test;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.OpenMultiBrowser;
import common.utils;
import pageObject.loginObject;

public class tc6_checkTheLostPassword {
	@Parameters("browser")
	@BeforeMethod
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
		utils.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
	
	@Test
	public void checkLostPass() throws InterruptedException {
		utils.driver.get(utils.URL);
		utils.driver.findElement(loginObject.lLostPass).click();
		Thread.sleep(3000);
		utils.driver.findElement(loginObject.txtUsername).sendKeys("nashtech");
		utils.driver.findElement(loginObject.textEmail).sendKeys("sangtbo@gmail.com");
		utils.driver.findElement(loginObject.btnSubmit).click();
		Thread.sleep(3000);
		if (utils.driver.getPageSource()
				.contains("If you supplied the correct username and e-mail address for"
						+ " your account, we will now have sent a confirmation message to that e-mail address. "
						+ "Once the message has been received, follow the instructions provided to change"
						+ " the password on your account.")) {
			ExcelCommon_POI.setCellData(3, 3, utils.excelData, utils.sheetMaster, "Pass");
		} else {
			ExcelCommon_POI.setCellData(3, 3, utils.excelData, utils.sheetMaster, "Failse");
		}
		utils.driver.findElement(loginObject.lProceed).click();
		Thread.sleep(3000);
		
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
