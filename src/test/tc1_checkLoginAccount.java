package test;

import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.OpenMultiBrowser;
import common.commonFuntions;
import common.utils;
import pageObject.loginObject;

public class tc1_checkLoginAccount {
	@Parameters("browser")
	@BeforeMethod
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
		utils.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void checkLoginAccount() throws Exception {
		utils.driver.get(utils.URL);
		
		XSSFSheet ExcelDataLogIn = ExcelCommon_POI.setExcelFile(utils.excelData, utils.sheetLogin);
		String Username = ExcelCommon_POI.getCellData(1, 1, ExcelDataLogIn);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataLogIn);

		utils.driver.findElement(loginObject.txtUsername).sendKeys(Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(Password);
		utils.driver.findElement(loginObject.btnLogin).click();
		Thread.sleep(3000);
		
		if (utils.driver.getPageSource().contains("Logged")) {
			ExcelCommon_POI.setCellData(1, 3, utils.excelData, utils.sheetMaster, "Pass");
		}else {
			ExcelCommon_POI.setCellData(1, 3, utils.excelData, utils.sheetMaster, "Fail");
		}
	}

	@AfterMethod
	public void after() throws InterruptedException {
		commonFuntions.commonLogOut();
		utils.driver.close();
	}
}
