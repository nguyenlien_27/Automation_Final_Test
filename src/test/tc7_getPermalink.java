package test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.OpenMultiBrowser;
import common.commonFuntions;
import common.utils;
import pageObject.homeObject;
import pageObject.viewIssuesObject;

public class tc7_getPermalink {
	@Parameters("browser")
	@BeforeMethod

	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
		utils.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFuntions.commonLogin();
	}

	@Test
	public void creatPermalink() throws Exception {

		// view issues list
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);

		// Click on create permalink
		utils.driver.findElement(viewIssuesObject.lCreatePermalink).click();
		Thread.sleep(3000);

		ArrayList<String> tabs2 = new ArrayList<String>(utils.driver.getWindowHandles());
		utils.driver.switchTo().window(tabs2.get(1));
		String permalink = utils.driver.findElement(By.xpath("html/body/div[2]/p/a")).getText();

		if (utils.driver.findElement(By.xpath("html/body/div[2]/p/a")).isDisplayed()) {
			System.out.println(permalink);
			ExcelCommon_POI.setCellData(1, 1, utils.excelData, utils.sheetPermalink, permalink);
			ExcelCommon_POI.setCellData(6, 3, utils.excelData, utils.sheetMaster, "Pass");
			utils.driver.close();
		} else {
			ExcelCommon_POI.setCellData(6, 3, utils.excelData, utils.sheetMaster, "Fail");
		}

		utils.driver.switchTo().window(tabs2.get(0));
	}

	@AfterTest
	public void after() throws InterruptedException {
		commonFuntions.commonLogOut();
		utils.driver.close();
	}
}
