package test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.OpenMultiBrowser;
import common.utils;
import pageObject.loginObject;
import pageObject.signupObject;

public class tc5_checkSignupNewAccount {
	@Parameters("browser")
	@BeforeMethod
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
		utils.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}	
	
	@Test
	public void singUp() throws InterruptedException {
		// get current date time
		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
		// get current date time with Date()
		Date date = new Date();

		// Now format the date
		String currentdatetime = dateFormat.format(date);
		
		
		utils.driver.get(utils.URL);
		utils.driver.findElement(loginObject.lSignUpNewAccount).click();
		Thread.sleep(3000);
		utils.driver.findElement(signupObject.txtUserName).sendKeys("liennt" + currentdatetime);
		utils.driver.findElement(signupObject.txtEmail).sendKeys("liennt" + currentdatetime + "@gmail.com");
		utils.driver.findElement(signupObject.btnSignUp).click();
		Thread.sleep(3000);
		if (utils.driver.getPageSource().contains("Congratulations. You have registered successfully.")) {
			ExcelCommon_POI.setCellData(2, 3, utils.excelData, utils.sheetMaster, "Pass");
		} else {
			ExcelCommon_POI.setCellData(2, 3, utils.excelData, utils.sheetMaster, "Fail");
		}
	}
	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
