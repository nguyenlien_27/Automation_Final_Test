package test;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.OpenMultiBrowser;
import common.commonFuntions;
import common.utils;
import pageObject.homeObject;

public class tc2_checkReportIssue {
	@Parameters("browser")
	@BeforeMethod
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
		utils.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFuntions.commonLogin();
	}

	@Test
	public static void reportIssue() throws Exception {

		// view issue
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);

		// search issue
		commonFuntions.commonSeachIssue();

		// issue is not found
		if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			// report issue
			commonFuntions.commonReportIssue();
			// search issue
			commonFuntions.commonSeachIssue();
			
			if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
				ExcelCommon_POI.setCellData(7, 3, utils.excelData, utils.sheetMaster, "Fail");
			} else {
				ExcelCommon_POI.setCellData(7, 3, utils.excelData, utils.sheetMaster, "Pass");
				commonFuntions.commonDeleteIssue();
			}
		} else {
			ExcelCommon_POI.setCellData(7, 3, utils.excelData, utils.sheetMaster, "Pass");
			commonFuntions.commonDeleteIssue();
		}
		
	}

	@AfterMethod
	public void after() throws InterruptedException {
		commonFuntions.commonLogOut();
		utils.driver.close();
	}

}
